# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 10:56:10 2020

@author: Maxi Schnabl
"""
necessary_flag = False   

def convert_temperature(x, flag1, flag2):
    global necessary_flag
    if(flag1 == flag2):
        print("\n No conversion necessary")
        necessary_flag = True
    elif(flag1 == 'C'):
        if(flag2 == 'F'):
            convTemp = (9/5)*x+32
            return convTemp
        elif(flag2 == 'K'):
            convTemp = (x+273.15)
            return convTemp
    elif(flag1=='K'):
        if(flag2=='C'):
            convTemp = (x-273.15)
            return convTemp
        elif(flag2=='F'):
            convTemp = x*9/5-459.67
            return convTemp
    elif(flag1=='F'):
        if(flag2=='C'):
            convTemp = 5/9*(x-32)
            return convTemp
        elif(flag2 == 'K'):
            convTemp = (x+459.57)*5/9
            return convTemp
        
        
unit1_acc = False
unit2_acc = False    
deg_acc = False 

print("\n This is a temperature converter, which can convert between Celsius, Fahrenheit and Kelvin.")

#reading and validating input unit 1 
unit1 = input(prompt="Please enter the unit you want to convert from? (C,K,F):")
if(unit1 == "C" or unit1 == "K" or unit1 == "F"):
    unit1_acc = True   
elif(unit1_acc == False):    
    while(unit1_acc==False):
        print("Please enter a valid unit (C, K, F)")
        unit1 = input(prompt="From which unit should the temperature be converted? (C,K,F):")
        if(unit1 == "C" or unit1 == "K" or unit1 == "F"):
            unit1_acc = True
            break
    
#reading and validating input unit 2    
unit2 = input(prompt="Please enter the unit you want to convert to? (C,K,F): ")
if(unit2 == "C" or unit2 == "K" or unit2 == "F"):
    unit2_acc = True
elif(unit2_acc == False):
    while(unit2_acc==False):
        print("Please enter a valid unit (C, K, F)")
        unit2 = input(prompt="To which unit should the temperature be converted? (C,K,F): ")
        if(unit2 == "C" or unit2 == "K" or unit2 == "F"):
            unit1_acc = True
            break

#reading and validating temperature value to convert
while(deg_acc == False):
    temp_input = input(prompt="Please enter the amount of degrees? (°): ")
    try:
        temp2conv = float(temp_input)
        deg_acc = True
        break
    except ValueError:
        print("Input Error! Please enter a number")
        
converted = convert_temperature(temp2conv, unit1, unit2)    
if(necessary_flag == False):
    print("\n", temp2conv, "°", unit1, "are", "%.2f"% converted,"°", unit2)
