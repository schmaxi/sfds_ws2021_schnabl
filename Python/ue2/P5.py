# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 10:33:14 2020

@author: Maxi Schnabl
"""

import math
import numpy as np

#with the trapezoidal rule one can approximate the integral of a given function
# by representing the curve by many trapezoids and calculating their area
def integrate_f(N, ival, func):
    a = ival[0]
    b = ival[1]
    h = (b-a)/N
    I = 0
    for i in range(1, N):
        x_i = a + (i-1) * h #x_i
        x_i1 = a + i * h    #x_i+1
        if(func == "xsquare"):
            I = I + h/2 * (x_i*x_i + x_i1*x_i1)
        elif(func == "sin"):
            I = I + h/2 * (math.sin(x_i)*math.sin(x_i) + math.sin(x_i1)*math.sin(x_i1))
    return I



N = np.array([100, 1000, 10000])

# --------- integrate sin^2 --------------
# trapezoidal rule
sin_res = np.array([0.0, 0.0, 0.0])
ival = ([0, (2*math.pi)])
f = "sin"
sin_res[0] = integrate_f(N[0], ival, f)
sin_res[1] = integrate_f(N[1], ival, f)
sin_res[2] = integrate_f(N[2], ival, f)

#numeric solution
a_sin = 0
b_sin = 2 * math.pi
I_sin = (1/2*(-math.cos(b_sin)*math.sin(b_sin)+b_sin))-(1/2*(-math.cos(a_sin)*math.sin(a_sin)+a_sin))  # numeric solution

for i in range(len(N)):
    print("Integral sin^2 calculated with the trapezoidal rule with accuracy N = ", N[i], "is", sin_res[i])
print("The numeric solution is: ", I_sin, "\n")


# --------- integrate x^2 --------------
# trapezoidal rule
x_res = np.array([0.0, 0.0, 0.0])
ival = np.array([2, 5])
f = "xsquare"
x_res[0] = integrate_f(N[0], ival, f)
x_res[1] = integrate_f(N[1], ival, f)
x_res[2] = integrate_f(N[2], ival, f)

#numeric solution
a_x = 2
b_x = 5
I_x = (b_x*b_x*b_x/3)-(a_x*a_x*a_x/3)

for i in range(len(N)):
    print("Integral x^2 calculated with the trapezoidal rule with accuracy N = ", N[i], "is", x_res[i])
print("The numeric solution is: ", I_x)

