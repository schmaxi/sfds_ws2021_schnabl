# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 18:53:10 2020

@author: Maxi Schnabl
"""

def bin_low_mid_high(number, mid_thresh, high_thresh):        
    if(number<=mid_thresh):
        return "low"
    elif(number > mid_thresh and number <= high_thresh):
        return "mid"
    elif(number > high_thresh):
        return "high"

        
thresh_flag = False         #flag to check threshold variables - low < high
ValInput = False            #Validating input - integers
mid_thresh_val = False      #validating lower threshold
high_thresh_val = False     #validating upper threshold
num_val = False             #validating number to categorize
stop = False                #flag to exit program

print("\nThis program categorizes numbers based on two thresholds entered by the user in low, mid and high. \nYou can edit the threshold variables by entering 'low' or 'high' for the lower and higher threshold respectively and view them by entering 'view'.")
while(stop == False): #loop to categorize more numbers, broken by userinput ('n)
    num_val = False
    while(ValInput == False):                #loop for validating input - check for integer
        while(thresh_flag == False):         # loop for validating threshold variables - low < high
            if(mid_thresh_val == False):     # validating lower threshold
                try:
                    low = int(input(prompt="Please enter the lower threshold: "))
                    mid_thresh_val = True
                except:
                    print("Please enter an integer")
                    mid_thresh_val = False
            if(high_thresh_val == False):   #validating upper threshold
                try:
                    high = int(input(prompt="Please enter the upper threshold: "))
                    high_thresh_val = True
                except:
                    print("Please enter an integer")
                    high_thresh_val = False
            if(high_thresh_val == True and mid_thresh_val == True): #validate threshold inputs
                if(low<high):
                    thresh_flag = True
                    break
                else:
                    print("User error: thresholds not strictly increasing")
                    mid_thresh_val = False
                    high_thresh_val = False
                    thresh_flag = False
        if(num_val == False):       #validating number to categorize and printing result
            try:
                number_input = int(input(prompt = "Enter a number to categorize:"))  
                num_val = True
                out = bin_low_mid_high(number_input, low, high)  
                print("\n",number_input, "is", out)
            except:
                print("Please enter an integer")
                
        #escaping loop if every input validated
        if(mid_thresh_val == True and high_thresh_val == True and num_val == True): 
            ValInput = True         
            break 
    escape = input(prompt = "Do you want to categorize another number, edit or view the thresholds? (y/n/low/high/view): ")
    if(escape == "n"):
        stop = True
        break
    if(escape == "low"):
        mid_thresh_val = False
        ValInput = False
        thresh_flag = False
    if(escape == "high"):
        high_thresh_val = False
        ValInput = False
        thresh_flag = False
    if(escape == "view"):
        print("The lower threshold is: ", low, ", the upper one: ", high)