# -*- coding: utf-8 -*-
"""
Created on Sun Oct 11 15:38:30 2020

@author: Maxi Schnabl
"""


def check_prime(num):
    isprime = False
    if num > 1: #prime numbers are greater than 1
        for i in range(2,num):
            if (num % i) == 0:
                #print(num, " is not a prime number, because:", num, "%", i, "=", num%i)
                isprime = False
                break
        else:
            isprime = True
            #print(num, "is a prime number.")
             
    else:
        isprime = False
        #print(num, "is not a prime number.")
    return isprime


#userInput = int(input(prompt="Please enter a number to check if its a prime: "))            
#check_prime(userInput)
    
    