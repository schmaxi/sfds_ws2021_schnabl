# -*- coding: utf-8 -*-
"""
Created on Sun Oct 11 17:17:13 2020

@author: Maxi Schnabl
"""

from P1 import check_prime

finVal = int(input(prompt = "Up to which number should the pairs be calculated? "))

pairs = []

def prime_twins(n):
    for p1 in range(n):
        if(check_prime(p1)):
            for p2 in range(n):
                if(check_prime(p2) and (p2-p1==2)):
                    pairs.append([p1,p2])
    return pairs
                    
prime_twins(finVal)
print("Pairs found: ")
for x in range(len(pairs)):
    print(pairs[x])
    