# -*- coding: utf-8 -*-
"""
@author: Maxi Schnabl
"""

import numpy as np

# clear command line and workspace:
try:
    from IPython import get_ipython
    get_ipython().magic('clear')
    get_ipython().magic('reset -f')
except:
    pass


list1 = [1,3,3,2,4,6, "d", "b", "a"]

#a)
#a1 = min(list1) #not supported between instances of 'str' and 'int'
#a2 = max(list1)
#a3 = sum(list1)

a = [[list1[0],list1[1],list1[2],list1[3],list1[4],list1[5]], [list1[6],list1[7],list1[8]]]
a_sum = sum(a[0])
a_min = min(a[0])
a_max = max(a[0])


#b)
list_num = list.copy(a[0])
list_char = list.copy(a[1])


#c)
list_num1 = list.copy(sorted(list_num))
list_char1 = list.copy(sorted(list_char))
print("Sorted numbers: ", list_num1, ", Sorted characters: ", list_char1)


#d) 
list_char2 = list.copy(list_char)
list_char2 = list_char2[::-1]  #umdrehen der Reihenfolge


#e)
#Methode 1
list_e1 = list.copy(list_num)
list_e1 = list(filter(lambda x: x < 4, list_e1)) #filter(Bedingung, liste)
#Methode 2
list_e2 = list.copy(list_num)
list_e2 = [x for x in list_e2 if x < 4]
  

#f)
list_f = list.copy(list_char1)
list_f2 = ["a", "c", "e"]
for i in range(len(list_f2)):
    for x in range(len(list_f)):
        if(list_f[x] >= list_f2[i]):
            list_f.insert(x, list_f2[i])
            break
        if(x == len(list_f)-1):
            list_f.append(list_f2[i])
            
        
