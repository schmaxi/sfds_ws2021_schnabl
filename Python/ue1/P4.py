# -*- coding: utf-8 -*-
"""
Created on Sat Oct 10 19:03:20 2020

@author: Maxi Schnabl
"""

Inventory = {"Apfel" : 3, "Banane" : 3, "Obstkorb" : ["Banane", "Apfel", "Birne"]}


#a)
Inventory["Weintraube"] = 4


#b)
Inventory["Obstkorb"].append("Weintraube")


#c)
Inventory["Obstkorb"].sort()


#d)
Inventory["Banane"] = Inventory["Banane"] + 10