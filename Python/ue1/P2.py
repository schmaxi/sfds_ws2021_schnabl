# -*- coding: utf-8 -*-
"""
Created on Sat Oct 10 16:51:07 2020

@author: Maxi Schnabl
"""

import numpy as np


#a)
a1 = [1,2,3,4]    #erstellt eine Liste mit 4 Elementen
a2 = [[1,2,3,4]]  #erstellt Liste mit einem Element [1,2,3,4]


#b)
b = [[1,2,3], [4,5,6]]


#c)
c = [1,2,3,4,5,6]
c1 = list.copy(c)
c1[2] = "Albert"


#d)
d1 = np.array([1,2,3,4,5,6])  
d2 = np.array(range(1, 7))


#e)
e = np.copy(d1)
#e[2] = "Einstein"  #Lässt sich nicht kopieren da es sich um ein Integer-Array
                    #handelt
                    
                    
#f)
f = np.copy(d1)
f[0] = 8.999  #Nachkommastellen werden abgeschnitten, da Typ int ist
f1 = np.array(np.copy(d1), dtype="float")
f1[0] = 8.999


#g)
g = np.copy(d1)
np.put(g, [0,1], [40, 50])


#h)
h = np.copy(d1)
np.put(h, [0,1,2,3], [40,50])
