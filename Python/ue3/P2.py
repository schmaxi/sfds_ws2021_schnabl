# -*- coding: utf-8 -*-
"""""""""""""""""""""""""""""""""""""""""""""""
Created on:     Mon Oct 19 09:12:25 2020
Author:         Maxi Schnabl
Company:        FH Joanneum GmbH
Department:     Applied Computer Science
Exercise:       UE3/P2 - Solving linear equation system
"""""""""""""""""""""""""""""""""""""""""""""""

import numpy as np

a = np.array([[1,2,1,5,-3,4],[1,-1,3,2,-1,3],[-2,1,-3,-1,2,1],[1,2,-1,-1,3,-6],[3,1,2,2,1,-3],[-1,-3,1,1,3,-2]])
b= np.array([4,20,-15,-3,16,-27])

x1 = np.linalg.solve(a, b)
x2 = np.linalg.inv(a).dot(b)
print("Solving the equation system with 'solve' results in:", x1, "\n Solving the equation system via the inverse matrix:", x2)

