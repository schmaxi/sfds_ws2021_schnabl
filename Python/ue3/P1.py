# -*- coding: utf-8 -*-
"""""""""""""""""""""""""""""""""""""""""""""""
Created on:     Tue Oct 13 10:33:14 2020
Author:         Maxi Schnabl
Company:        FH Joanneum GmbH
Department:     Applied Computer Science
Exercise:       UE3/P1 - Calculating determinant of 2x2 or 3x3 matrices
"""""""""""""""""""""""""""""""""""""""""""""""

import numpy as np

def my_det(A):
    if A.shape == (2,2):
        det = (A[0][0] * A[1][1]) - (A[0][1] * A[1][0])
        print("Die Determinante der 2x2 Matrix ist: ", det)
    elif A.shape == (3,3):
        det = (A[0][0] * A[1][1] * A[2][2]) + (A[0][1] * A[1][2] * A[2][0]) + (A[0][2] * A[1][0] * A[2][1]) - (A[0][2] * A[1][1] * A[2][0]) - (A[0][1] * A[1][0] * A[2][2]) - (A[0][0] * A[1][2] * A[2][1])
        print("Die Determinante der 3x3 Matrix ist: ", det)
    else:
        print("Fehler, Falsche Dimensionen! Die Determinante kann nicht berechnen werden.")


mat2 = np.array([[2,5], [9,1]])
my_det(mat2)

mat3 = np.array([[4,1,8], [2,5,4], [1,7,3]])
my_det(mat3)

mat0 = np.array([[1,5,2], [3,9,2]])
my_det(mat0)