# -*- coding: utf-8 -*-

"""""""""""""""""""""""""""""""""""""""""""""""
Created on:     Mon Oct 19 10:26:39 2020
Author:         Maxi Schnabl
Company:        FH Joanneum GmbH
Department:     Applied Computer Science
Exercise:       UE3/P4 - Rock, Paper, Scissor
"""""""""""""""""""""""""""""""""""""""""""""""

import random           #for randint() function

run_flag = True         #flag for validating another round
input_flag = False       #flag for validating user input


def gen_comp_choice(CompChoice):
    if CompChoice == 0:
        CompInput = "rock"
        return CompInput    
    elif CompChoice == 1:
        CompInput = "paper"
        return CompInput
    elif CompChoice == 2:
        CompInput = "scissor"
        return CompInput

        
def eval_winner(comp, user):
    if (comp == "rock" and user == "paper"):
        winner = "user"
        return winner
    elif (comp == "rock" and user == "scissor"):
        winner = "computer"
        return winner
    elif(comp == "paper" and user == "rock"):
        winner = "computer"
        return winner
    elif(comp == "paper" and user == "scissor"):
        winner = "user"
        return winner
    elif(comp == "scissor" and user == "rock"):
        winner = "user"
        return winner
    elif(comp == "scissor" and user == "paper"):
        winner = "computer"
        return winner
    elif(comp == user):
        winner = "none"
        return winner
        

print("With this program one can play 'rock, paper and scissor' against the computer. The user simply just need to enter 'rock', 'paper' or 'scissor', the computer generates its 'choice' randomly.")
while(run_flag):                             #loop for playing another round
   while(input_flag == False):               #loop vor validating user input
       UserInput = input(prompt = "Please enter your choice ('rock', 'paper' or 'scissor'): ")
       if(UserInput != "rock" and UserInput != "paper" and UserInput != "scissor"):
           print("InputError! Please enter 'rock', 'paper' or 'scissor'")
           InputFlag = False
       else:
           InputFlag = True
           break    
   OppChoice =  gen_comp_choice(random.randint(0,2))
   win = eval_winner(OppChoice, UserInput)
   if(win != "none"):
       print("The winner is", win, "beccause the computer chose", OppChoice, "and the user chose", UserInput)
   elif(win == "none"):
       print("Its a draw! Both players chose", UserInput)
   cont = input(prompt = "Do you want to play another round? (y/n): ")    
   if(cont == "n"):
       run_flag = False
       break
        