# -*- coding: utf-8 -*-
"""""""""""""""""""""""""""""""""""""""""""""""
Created on:     Mon Oct 19 18:12:00 2020
Author:         Maxi Schnabl
Company:        FH Joanneum GmbH
Department:     Applied Computer Science
Exercise:       UE3/P3 - Monte Carlo Approximation
"""""""""""""""""""""""""""""""""""""""""""""""

import math
import random

#a) 
def is_in_circle(x, y):
    if(math.sqrt(x**2+y**2)<=1):
        return True
    else:
        return False
    
#b)
def simulate():
    x = random.uniform(-1,1)    
    y = random.uniform(-1,1)    
    res = is_in_circle(x,y)
    # if(res):
    #     print_string = "in circle"
    # else:
    #     print_string = "not in circle"
    #print("The Point (", x,",", y, ") is", print_string)
    return res

range_list = [1000, 10000, 100000, 1000000]
result_list = []

for r in range_list:
    results = list(map(lambda x: simulate(), range(r)))
    true_cnt = 0
    for item in results:
        if item == True:
            true_cnt = true_cnt + 1 
    pi_approx = (true_cnt / r * 4)
    result_list.append(pi_approx)
        

