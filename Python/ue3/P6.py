# -*- coding: utf-8 -*-
"""""""""""""""""""""""""""""""""""""""""""""""
Created on:     Mon Oct 19 11:46:05 2020
Author:         Maxi Schnabl
Company:        FH Joanneum GmbH
Department:     Applied Computer Science
Exercise:       UE3/P6 - List Comprehensions
"""""""""""""""""""""""""""""""""""""""""""""""

import numpy as np


#a)
a = ["test", "string", 1, 5, 3, 9, "extract"]
stringlist = [item for item in a if type(item) == str]

print(stringlist)


#b)
b = [x**2 for x in range(4,11) if x % 2 == 0]
print(b)


#c)
sublists = [[2,9,4], ["Hans", "Peter", "Wurst", "Klaus"], [2,9], [45,1,5,3]]
extracted = [list[1] for list in sublists if len(list)>=3]
print(sublists)
print(extracted)


#d)
lists = np.array([[2, 3, 4], [6, 7, 8], [7, 8, 9]])
lists_sqr = np.array([[y ** 2 for y in x] for x in lists])
print(lists_sqr)
