"""""""""""""""""""""""""""""""""""""""""""""""
Created on:     21.11.2020
Author:         Maxi Schnabl
Company:        FH Joanneum GmbH
Department:     Applied Computer Science
Exercise:       UE12/P2 - Matplotlib
"""""""""""""""""""""""""""""""""""""""""""""""

import matplotlib.pyplot as plt
import pandas as pd

ComProgLang = pd.read_csv("CommonProgrammingLang.csv", encoding='unicode_escape')

#langs = ComProgLang.groupby("LanguagesWorkedWith").count(level="LanguagesWorkedWith")

df = pd.Series([item for sub in ComProgLang["LanguagesWorkedWith"] for item in sub])
#df = pd.Series(sum([item for item in ComProgLang["LanguagesWorkedWith"]], [])).value_counts()

print(ComProgLang["LanguagesWorkedWith"][0][1])

#plt.bar(langs[0], langs[1])
#plt.show()

#print(test)
