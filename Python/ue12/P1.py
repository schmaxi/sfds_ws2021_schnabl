"""""""""""""""""""""""""""""""""""""""""""""""
Created on:     20.11.2020
Author:         Maxi Schnabl
Company:        FH Joanneum GmbH
Department:     Applied Computer Science
Exercise:       UE12/P1 - Matplotlib Pie-Chart
"""""""""""""""""""""""""""""""""""""""""""""""

import pandas as pd
import matplotlib.pyplot as plt

menu = pd.read_csv("menu.csv", encoding='unicode_escape')

cats = menu["Category"].value_counts()
explode = (0,0,0,0,0,0,0.5,0,0)

fig1, ax1 = plt.subplots()
ax1.pie(cats, labels=cats.index, explode=explode, autopct='%1.1f%%')
ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

plt.show()

