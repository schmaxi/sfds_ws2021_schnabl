"""""""""""""""""""""""""""""""""""""""""""""""
Created on:     08.11.2020
Author:         Maxi Schnabl
Company:        FH Joanneum GmbH
Department:     Applied Computer Science
Exercise:       UE7/P4 - Classes: n-D-Points
"""""""""""""""""""""""""""""""""""""""""""""""

import math


class Point(object):

    def __init__(self, input):
        self.len = (len(input))
        self.data = []
        for i in range(self.len):
            self.data.append(input[i])

    def __str__(self):
        return "%s"%(self.data)

    def distance(self, other):
        if self.len != other.len:
            raise Exception("Points must be in same dimension")
        else:
            sum = 0
            for i in range(self.len):
                sum = sum + ((other.data[i]-self.data[i])**2)
            dist = math.sqrt(sum)
            return dist

    def print_points(self, other):
        print("Point 1 is: ", P1, "\nPoint 2 is: ", P2, sep="")
        return 0

    def add_points(self, other):
        if self.len != other.len:
            raise Exception("Points must be in same dimension")
        else:
            added = []
            for i in range(self.len):
                added.append((self.data[i])+other.data[i])
            return Point(added)

    def sub_points(self, other):
        if other.len != self.len:
            raise Exception("Points must be in same dimension")
        else:
            subbed = []
            for i in range(self.len):
                subbed.append((self.data[i])-other.data[i])
            return Point(subbed)


class Point2d(Point):
    def __init__(self, point):
        self.x = point[0]
        self.y = point[1]
        super().__init__([self.x, self.y])


# P1 = Point([1,2,2,6,3])
# P2 = Point([1,2,2,6,3])
P1 = Point2d([1, 2])
P2 = Point2d([4, 3])

Point.print_points(P1, P2)

distance = Point.distance(P1, P2)
print("Distance between P1 and P2 is: ", distance)

addedPoints = Point.add_points(P1, P2)
print("Adding the points results in: ", addedPoints)

subbedPoints = Point.sub_points(P1, P2)
print("Subtracting the Points results in: ", subbedPoints)