"""""""""""""""""""""""""""""""""""""""""""""""
Created on:     22.10.2020
Author:         Maxi Schnabl
Company:        FH Joanneum GmbH
Department:     Applied Computer Science
Exercise:       UE6/P2 - Classes 2: Fractions
"""""""""""""""""""""""""""""""""""""""""""""""


class MyFrac:
    def __init__(self, nume, denume):
        self.nume = nume
        if denume == 0:
            raise Exception("Error! Denumerator is zero!")
        else:
            self.denume = denume

    def my_mul(self, other):
        _nume = self.nume * other.nume
        _denume = self.denume * other.denume
        return MyFrac(_nume, _denume)

    def my_div(self, other):
        _nume = self.nume * other.denume
        _denume = self.denume * other.nume
        return MyFrac(_nume, _denume)

    def my_add(self, other):
        if self.denume == other.denume:
            _nume = self.nume + other.nume
            _denume = self.denume
        else:  # same denumerator needed
            _denume = self.denume * other.denume
            _nume = self.nume * other.denume + other.nume * self.denume
        return MyFrac(_nume, _denume)

    def my_sub(self, other):
        if self.denume == other.denume:
            _nume = self.nume - other.nume
            _denume = self.denume
        else:  # same denumerator needed
            _denume = self.denume * other.denume
            _nume = self.nume * other.denume - other.nume * self.denume
        return MyFrac(_nume, _denume)

    def gcd(self): # greatest common divisor
        a = self.nume
        b = self.denume
        if a == 0:
            return b
        else:
            while b != 0:
                if a > b:
                    a = a - b
                else:
                    b = b - a
        return a

    def my_reduc(self):
        divisor = MyFrac.gcd(self)
        if divisor == self.denume or divisor == self.nume:
            print("Fraction", self, "cannot be shortened")
            return 0
        else:
            return MyFrac(int(self.nume / divisor), int(self.denume / divisor))

    def __str__(self):
        return "(%s/%s)"%(self.nume, self.denume)


x = MyFrac(1, 6)
y = MyFrac(4, 2)

print("Fractions are: ", x,", ", y, sep = "")

mul = MyFrac.my_mul(x, y)
print("multiplied: ", mul)

div = MyFrac.my_div(x, y)
print("divided: ", div)

added = MyFrac.my_add(x, y)
print("added: ", added)

sub = MyFrac.my_sub(x, y)
print("subtracted: ", sub)

shortened = MyFrac.my_reduc(x)
if shortened:
    print("Fraction 1 shortened is: ", shortened)