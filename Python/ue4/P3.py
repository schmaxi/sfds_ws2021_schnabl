"""""""""""""""""""""""""""""""""""""""""""""""
Created on:     22.10.2020
Author:         Maxi Schnabl
Company:        FH Joanneum GmbH
Department:     Applied Computer Science
Exercise:       UE6/P3 - Classes: 2d Points
"""""""""""""""""""""""""""""""""""""""""""""""

import math


class Point():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return "(%s,%s)"%(self.x, self.y)

    def dist(self, other):
        _dist = math.sqrt(((other.x - self.x)**2)+((other.y - self.y)**2))
        return _dist

    def print_points(self, other):
        print('Point is 1: (', self.x, ',', self.y, '), Point 2 is: (', other.x, ',', other.y, ')', sep = '')

    def add_points(self, other):
        x = self.x + other.x
        y = self.y + other.y
        return Point(x, y)

    def sub_points(self, other):
        x = self.x - other.x
        y = self.y - other.y
        return Point(x, y)

    def __add__(self, other):
        return self.x + other.x, self.y + other.y

    def __sub__(self, other):
        return self.x - other.x, self.y - other.y


# P1 = Point(1, 2)
# P2 = Point(4, 3)
#
# distance = Point.dist(P1, P2)
#
# added = Point.add_points(P1, P2)
# subbed = Point.sub_points(P1, P2)
#
# #Point.print_points(P1, P2)
# print('Distance between points is: ', distance)
# #print('Adding the two points results in: (', added.x, ',', added.y, ')', sep = '')
# #print('Subtracting results in: (', subbed.x, ',', subbed.y, ')', sep = '')
#
# print('Added: ', P1 + P2)
# print('Subtracted: ', P1 - P2)