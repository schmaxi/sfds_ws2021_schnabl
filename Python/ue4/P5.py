"""""""""""""""""""""""""""""""""""""""""""""""
Created on:     08.11.2020
Author:         Maxi Schnabl
Company:        FH Joanneum GmbH
Department:     Applied Computer Science
Exercise:       UE6/P5 - Classes: Triangle
"""""""""""""""""""""""""""""""""""""""""""""""

from Python.ue4.P3 import Point
import math


class Triangle(object):
    def __init__(self, point_A, point_B, point_C):
        # defining the Points
        self.A = point_A
        self.B = point_B
        self.C = point_C
        # calculating length of the sides
        self.a = Point.dist(self.B, self.C)
        self.b = Point.dist(self.C, self.A)
        self.c = Point.dist(self.A, self.B)

        self.s = ((self.a + self.b + self.c) / 2)

    def __str__(self):
        return "%s, %s and %s" % (self.A, self.B, self.C)

    def perimeter(self):
        return self.a + self.b + self.c

    def area(self):
        return math.sqrt(self.s * (self.s - self.a) * (self.s - self.b) * (self.s - self.c))  # Heron's theorem

    def is_right_angle(self):
        h_a = ((2 / self.a)*math.sqrt(self.s * (self.s - self.a) * (self.s - self.b) * (self.s - self.c)))
        h_c = ((2 / self.c) * math.sqrt(self.s * (self.s - self.a) * (self.s - self.b) * (self.s - self.c)))
        alpha = math.degrees(math.asin(h_c/self.b))
        beta = math.degrees(math.asin(h_a/self.c))
        gamma = 180-(alpha + beta)
        print("alpha is: ", alpha, "\nbeta is :", beta, "\ngamma is :", gamma)
        if round(alpha, 0) == 90 or round(beta, 0) == 90 or round(gamma, 0) == 90:
            return True
        else:
            return False


P1 = Point(0, 0)
P2 = Point(1, 3)
P3 = Point(1, 1)

T1 = Triangle(P1, P2, P3)

print("The Triangle consists of the points :", T1)

perimeter = Triangle.perimeter(T1)
print("Perimeter is: ", perimeter)

area = Triangle.area(T1)
print("Area is: ", area)

if Triangle.is_right_angle(T1):
    print("The given Triangle has a right angle")
else:
    print("The given Triangle has no right angle")
