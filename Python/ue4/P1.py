"""""""""""""""""""""""""""""""""""""""""""""""
Created on:     22.10.2020
Author:         Maxi Schnabl
Company:        FH Joanneum GmbH
Department:     Applied Computer Science
Exercise:       UE6/P1 - Classes 1: mathematical operations
"""""""""""""""""""""""""""""""""""""""""""""""


class MyCalculations:
    def __init__(self, a):
        self.a = a

    def my_mul(self, other) -> int:
        return self.a * other.a

    def my_div(self, other) -> float:
        return self.a / other.a

    def my_add(self, other) -> int:
        return self.a + other.a

    def my_sub(self, other) -> int:
        return self.a - other.a


x = MyCalculations(7)
y = MyCalculations(5)
add = MyCalculations.my_add(x, y)
div = MyCalculations.my_div(x, y)
mul = MyCalculations.my_mul(x, y)
sub = MyCalculations.my_sub(x, y)

print(add, div, mul, sub)
