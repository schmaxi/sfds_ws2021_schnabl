%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Date     = 09.12.2020
% Author   = Schnabl Maximilian
% Exercise = UE13/M3 - functions 3 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc 
%%

i_sin(1) = integrate_f(100  , 0, 2*pi, "sin");
i_sin(2) = integrate_f(1000 , 0, 2*pi, "sin");
i_sin(3) = integrate_f(10000, 0, 2*pi, "sin");

i_x(1) = integrate_f(100  , 2, 5, "x");
i_x(2) = integrate_f(1000 , 2, 5, "x");
i_x(3) = integrate_f(10000, 2, 5, "x");



%% analytic solutions:
% sin^2(x):
a = 2*pi;
I_sin = 1/2*(a-2*sin(a)*cos(a))-(1/2*(0-2*sin(0)*cos(0)));

% x^2
a = 2;
b = 5;       
I_x = b*b*b/3-a*a*a/3;


%% numeric solution:
% integration using the trapezoidal rule approximates the area under the
% curve using multiple trapezoids
function [val] = integrate_f(N,a,b,f)
    h = (b-a)/N;
    I = 0;
    for i =1:N
        x_i = a + (i-1)*h;  
        x_i1 = a + i*h;     
        if (f == "sin")
            I = I + h/2*(sin(x_i)*sin(x_i) + sin(x_i1)*sin(x_i1));
        elseif(f == "x")
            I = I + h/2*(x_i*x_i + (x_i1)*(x_i1));
        end
    end
    val = I;
end
