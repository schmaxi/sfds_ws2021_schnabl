%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Date     = 09.12.2020
% Author   = Schnabl Maximilian
% Exercise = UE13/M1 - functions 1 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc 
%%

convert_temperature(0,"K","K")
convert_temperature(0,"K","C")
convert_temperature(0,"K","F")

convert_temperature(0,"C","C")
convert_temperature(0,"C","K")
convert_temperature(0,"C","F")

convert_temperature(0,"F","F")
convert_temperature(0,"F","K")
convert_temperature(0,"F","C")


%% Functions

function [temp] = convert_temperature(x, flag1, flag2)
    if(flag1 == "K")
        if(flag2 == "K")
            temp = "No conversion necessary";
        elseif(flag2 == "C")
            temp = x - 273;
        elseif(flag2 == "F")
            temp = (x-273)*9/5+32;
        else
            temp = "User Error! Invalid arguments";
        end
    end

    if(flag1 == "C")
        if(flag2 == "C")
            temp = "No conversion necessary";
        elseif(flag2 == "K")
            temp = x + 273;
        elseif(flag2 == "F")
            temp = (x)*9/5+32;
        else
            temp = "User Error! Invalid arguments";
        end
    end

    if(flag1 == "F")
        if(flag2 == "F")
            temp = "No conversion necessary";
        elseif(flag2 == "K")
            temp = (x-32)*5/9+273;
        elseif(flag2 == "C")
            temp = (x-32)*5/9;
        else
            temp = "Invalid arguments";
        end
    end
end




