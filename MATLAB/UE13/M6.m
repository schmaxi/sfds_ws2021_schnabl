%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Date     = 09.12.2020
% Author   = Schnabl Maximilian
% Exercise = UE13/M6 - matrices calculation 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc 
%%

A = [5,9;
    7,3;
    2,6];

B = [2,5,6;
    7,1,3];

C = [1,5,2,4;
    6,2,3,5];

D = mat_mul(A,B)

E = mat_mul(A,C)


%% functions

function [mat] = mat_mul(A,B)
    [arows, acols] = size(A)
    [brows, bcols] = size(B)
    if(arows == bcols)
        C = zeros(arows,bcols);
        for i = 1:arows           %iterates columns of A
            for j = 1:bcols        %iterates rows of B
                sum = 0;
                for k = 1:acols    %iterates rows of A
                    sum = sum + A(i,k)*B(k,j);
                end
                C(i,j) = sum;
            end
        end
        mat = C;
    else
        mat = warning("Dimension Error!");
    end
end
