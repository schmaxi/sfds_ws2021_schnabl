%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Date     = 09.12.2020
% Author   = Schnabl Maximilian
% Exercise = UE13/M2 - functions 2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc 
%%

bin_low_mid_high(4, 3, 1)
bin_low_mid_high(1, 2, 5)
bin_low_mid_high(3, 2, 5)
bin_low_mid_high(7, 2, 5)


%% Function

function [cat] = bin_low_mid_high(number, mid_thresh, high_thresh)
    if(mid_thresh >= high_thresh)
        cat = "User error: thresholds not strictly inceasing";
    else
        if(number < mid_thresh)
            cat = "low";
        elseif(number >= mid_thresh && number < high_thresh)
            cat = "mid";
        elseif(number >= high_thresh)
            cat = "high";
        end
    end
end

      