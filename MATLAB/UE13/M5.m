%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Date     = 10.12.2020
% Author   = Schnabl Maximilian
% Exercise = UE13/M5 - functions 5 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc 
%%

A = [4,2,3;
    4,5,6;
    7,8,9];

B = [5,2;
    4,7];

C = [1,3;
    4,1;
    6,4];

fprintf("A:")
det_a = my_det(A)
det(A)

fprintf("B:")
det_b = my_det(B)
det(B)

fprintf("C:")
det_c = my_det(C)


%% functions
function [A] = det_2x2(X)
    A = X(1,1)*X(2,2) - X(2,1)*X(1,2); 
end

function [A] = det_3x3(X)
    a = X(1,1)*X(2,2)*X(3,3) + X(1,2)*X(2,3)*X(3,1) + X(1,3)*X(2,1)*X(3,2);
    b = -X(3,1)*X(2,2)*X(1,3) - X(3,2)*X(2,3)*X(1,1) - X(3,3)*X(2,1)*X(1,2);
    A = a + b;
end

function [B] = my_det(A)
    [nrows, ncols] = size(A);
    if(nrows == 2 && ncols == 2)
        B = det_2x2(A);
    elseif(nrows == 3 && ncols == 3)
        B = det_3x3(A);
    else
        disp("Dimension Error: determinant could not be calculated!");
        B = NaN;
    end
end
