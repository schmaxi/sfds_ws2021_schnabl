%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Date     = 09.12.2020
% Author   = Schnabl Maximilian
% Exercise = UE13/M4 - functions 4 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc 
%%

n = 100;
fprintf("n=100: \nSum1:")
sum1(n)
fprintf("Sum2:")
sum2(n)

n = 1000;
fprintf("n=1000: \nSum1:")
sum1(n)
fprintf("Sum2:")
sum2(n)

n = 10000;
fprintf("n=10000: \nSum1:")
sum1(n)
fprintf("Sum2:")
sum2(n)

%% Functions

function [sum] = sum1(n)
    sum = 0;
    for i = 1:n
        sum = sum + i;
    end
end

function [sum] = sum2(n)
    sum = n*(n+1)/2;
end
