"""""""""""""""""""""""""""""""""""""""""""""""
Created on:     04.11.2020
Author:         Maxi Schnabl
Company:        FH Joanneum GmbH
Department:     Applied Computer Science
Exercise:       UE7/P2 - read multiple files
"""""""""""""""""""""""""""""""""""""""""""""""

import os
import pandas as pd
import fnmatch

#change directory
os.chdir("Beispiel5")
dir = os.getcwd()

# get filenames in directory
files = os.listdir(dir)

csv_files = []
for file in os.listdir(dir):
    if fnmatch.fnmatch(file, "*.csv"):
        csv_files.append(pd.read_csv(file, encoding= 'unicode_escape'))

#print(csv_files)