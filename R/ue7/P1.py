"""""""""""""""""""""""""""""""""""""""""""""""
Created on:     04.11.2020
Author:         Maxi Schnabl
Company:        FH Joanneum GmbH
Department:     Applied Computer Science
Exercise:       UE7/P1 - csv in python
"""""""""""""""""""""""""""""""""""""""""""""""

import os
import pandas as pd
import numpy as np
import fnmatch

dir = os.getcwd()

for file in os.listdir(dir):
    if fnmatch.fnmatch(file, "P1.csv"):
        P1_csv = pd.read_csv(file, encoding= 'unicode_escape')

#saving columns in np-arrays
manufacturer = np.array(P1_csv.manufacturer)
model = np.array(P1_csv.model)
displ = np.array(P1_csv.displ)

print(manufacturer)
#print(model)
#print(displ)