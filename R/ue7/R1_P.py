"""""""""""""""""""""""""""""""""""""""""""""""
Created on:     Sun Nov 01 11 2020
Author:         Maxi Schnabl
Company:        FH Joanneum GmbH
Department:     Applied Computer Science
Exercise:       UE7/R1 - load csv files
"""""""""""""""""""""""""""""""""""""""""""""""

import pandas as pd
import os
import fnmatch
import csv


dir = os.getcwd()

# find file including "house" in its name and ending with ".csv"
for file in os.listdir(dir):
    if fnmatch.fnmatch(file, "*house*.csv"):
        # read csv via pandas
        houses_pandas = pd.read_csv(file, encoding= 'unicode_escape', skiprows = [i for i in range(1, 1307)])

        # read via csv package
        with open(file,encoding= 'unicode_escape') as csvdatei:
            csv_reader = csv.reader(csvdatei, delimiter = ",")
            houses_csv = list(csv_reader)
        del houses_csv[1:1307]

#print(houses_pandas)
print(houses_csv[0:4])