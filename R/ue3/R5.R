"#################################################
Created on:     Mon Oct 19 10 2020
Author:         Maxi Schnabl
Company:        FH Joanneum GmbH
Department:     Applied Computer Science
Exercise:       UE3/R5 - Tibble 2 
#################################################"

rm(list = ls())


library(tidyverse)

teacher <- tibble(
  ID = 1:5,
  Vorname = c("Hans", "J�rg", "Olga", "Chantal", "Klaus"),
  Nachname = c("Mustermann", "Musterherr", "Musterfrau", "Musterdame", "Musterkind"),
  Benutzername = c("mhans85", "mjoer75", "molga77", "mchan42", "mklau18"),
  Alter = c(35,45,43,78,6),
  Geschlecht = c("m","m","w","w","m")
)


classes <- tibble(
  t_ID = 1:5,
  Fach = c("Mathe", "Informatik", "Geschichte", "Turnen", "Physik")
)


school <- tibble(bind_cols(teacher, classes))
school <- school %>% add_row(ID = 6, Vorname = "Ernst", Nachname = "Isdas", Benutzername = "iern90", Alter = 30, Geschlecht = "m", Fach = "Englisch")
school


classes <- classes %>% add_row(t_ID = 8) %>% add_row(t_ID = 9)
inner_join(school, classes)  # includes all rows in x and y
left_join(school, classes)   # inludes all rows in x
right_join(school, classes)  # includes all rows in y
semi_join(school, classes)   # includes all rows in x with a match in y
