rm(list = ls())
#a)
a <- c(1:10)


#b)
sum(a)
min(a)
max(a)
median(a)
sd(a)


#c)
summary(a) #summary gibt Minimalwert, Maximalwert, Median und Mittelwert an


#d)
as.integer(a)  #as.integer wandelt alle Zahlen in integer Werte um

#e)
a[5] <- 5.55


#f) 
a[c(3,8)] <- c(2,19)


#g)
a[1] <- "hallo" # es werden alle Elemente des Vektors zu einem String
