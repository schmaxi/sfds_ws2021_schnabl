"#################################################
Created on:     Tue Oct 28 10 2020
Author:         Maxi Schnabl
Company:        FH Joanneum GmbH
Department:     Applied Computer Science
Exercise:       UE5/R4 - Tibble operations
#################################################"

rm(list = ls())

library(outbreaks)
library(tidyverse)
library(lubridate)

covid19_en <- as_tibble(covid19_england_nhscalls_2020)


#reported cases per day
CasesPerDay <- aggregate(covid19_en$count, by = list(covid19_en$date), FUN = sum)
plot(CasesPerDay, xlab = "Date", ylab = "Cases")


#extracting weekdays
dates <- as.Date(covid19_en$date, format = "%Y/%M/%D")
covid19_en$weekdays <- weekdays(dates)
CasesPerWeekday <- aggregate(covid19_en$count, by = list(covid19_en$weekdays), FUN = sum)
CasesPerWeekday <- subset(CasesPerWeekday, CasesPerWeekday$Group.1 != "Samstag" & CasesPerWeekday$Group.1 != "Sonntag")


#nhsregion vs sex
nhs_sex <- spread(covid19_en, nhs_region, sex) #meisten unkown in Midlands
#Frage: max-Wert von unknown extrahieren - unkown nicht "ansprechbar" 


#midlands
midlands <- filter(covid19_en, nhs_region == "Midlands")
midlands1 <- aggregate(midlands$count, by = list(midlands$ccg_name), FUN = sum)
max(midlands1$x) #Frage: Namen mit ausgeben?

NameSex <- midlands %>%
  group_by(ccg_name, sex) %>%
  sum(count)  # Frage: "only defined on a data frame with all numeric variables" 

