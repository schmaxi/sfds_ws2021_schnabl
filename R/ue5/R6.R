"#################################################
Created on:     Tue Oct 28 10 2020
Author:         Maxi Schnabl
Company:        FH Joanneum GmbH
Department:     Applied Computer Science
Exercise:       UE5/R6 - Lubridate
#################################################"

rm(list = ls())

library(lubridate)
library(nycflights13)

flights <- nycflights13::flights

flights_new <- flights %>% filter(arr_time < dep_time) %>%  
  mutate(arr_time_new = ymd_hm(paste(year,str_pad(month,2,"left","0"),
                                  str_pad(day,2,"left","0"),
                                  str_pad(arr_time,4,"left","0"),sep=""))+days(1))
  # ymd_hm - parse date-times with Year, Month, Day, Hour and Minute
  # paste - converts int to char


