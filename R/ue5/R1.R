"#################################################
Created on:     Tue Oct 27 10 2020
Author:         Maxi Schnabl
Company:        FH Joanneum GmbH
Department:     Applied Computer Science
Exercise:       UE5/R1 - Collatz-Problem
#################################################"

rm(list = ls())

collatz <- function(n){
  run = TRUE
  loop_cnt = 0
  while(run){
    if(n == 1){
      run = FALSE
      break
    }
    else if(n %% 2 == 0){
      m = n / 2
    }else if(n %% 2 != 0){
      m = 3*n + 1
    }
    loop_cnt = loop_cnt + 1 
    n = m
  }
  loop_cnt
}

print(collatz(1000))
