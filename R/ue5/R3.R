"#################################################
Created on:     Tue Oct 20 10 2020
Author:         Maxi Schnabl
Company:        FH Joanneum GmbH
Department:     Applied Computer Science
Exercise:       UE5/R3 - Tidy-Data
#################################################"


library(tidyverse)

#tidy: 1.: each variable has its own column, 
#      2.: each observation has its own row 
#      3.: each value has its own cell

# Titanic passenger counts by Class and Sex -------------------------------

titantic_passengers <-
  apply(Titanic, c("Class", "Sex"), sum) %>%
  as.data.frame() %>%
  rownames_to_column() %>%
  rename(Class = rowname) %>%
  as_tibble()


# --> is tidy



# Diamond auction prices --------------------------------------------------

diamond_prices <-
  diamonds %>%
  select(carat:clarity, price)

# --> is tidy


# Age and height of different people --------------------------------------

people <- tribble(
  ~name,             ~property,    ~value,
  #-----------------|--------|------
  "Phillip Woods",   "age",       45,
  "Phillip Woods",   "height",   186,
  "Phillip Woods",   "age",       50,
  "Jessica Cordero", "age",       37,
  "Jessica Cordero", "height",   156
)

# --> is not tidy, cant be transformed - age of Phillip Woods is duplicate with different values